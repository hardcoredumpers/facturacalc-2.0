import controller.ButtonController;
import model.Factura;
import view.MainWindow;

public class Main {
    public static void main(String[] args) {
        MainWindow view = new MainWindow();

        Factura factura = new Factura();

        ButtonController b = new ButtonController(factura, view);

        view.assignButtonController(b);

        view.setVisible(true);
    }
}
