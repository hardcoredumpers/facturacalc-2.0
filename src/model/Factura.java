package model;


import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Factura {
    private double base;
    private double iva;
    private double irpf;
    private double total;

    public Factura(double base) {
        this.base = base;
        this.iva = base * 0.21;
        this.irpf = base * 0.07;
        this.total = this.base + this.iva - this.irpf;
    }

    public Factura() {

    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        this.base = Double.parseDouble(df.format(base));
        this.iva = Double.parseDouble(df.format(base * 0.21));
        this.irpf = Double.parseDouble(df.format(base * 0.07));
        this.total = Double.parseDouble(df.format(this.base + this.iva - this.irpf));
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public double getIrpf() {
        return irpf;
    }

    public void setIrpf(double irpf) {
        this.irpf = irpf;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void calculaBase(double total) {
        setBase(total / 1.14);
    }
}
