package controller;

import model.Factura;
import view.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonController implements ActionListener {

    private Factura model;
    private MainWindow view;

    public ButtonController(Factura model, MainWindow view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        if (action.equals("base")) {
            double base = Double.parseDouble(view.getBase());
            model.setBase(base);
            view.showDetailsFactura(model.getBase(), model.getIva(), model.getIrpf(), model.getTotal());
        } else {
            if (action.equals("total")) {
                double total = Double.parseDouble(view.getTotalFactura());
                model.calculaBase(total);
                view.showDetailsFactura(model.getBase(), model.getIva(), model.getIrpf(), model.getTotal());
            }
        }
    }
}
