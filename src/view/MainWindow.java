package view;

import controller.ButtonController;

import javax.swing.*;
import java.awt.*;


public class MainWindow extends JFrame {

    private JButton jbBaseImponible;
    private JButton jbTotalFactura;
    private JTextField jtfBaseImponible;
    private JTextField jtfTotalFactura;


    public MainWindow() {
        JTabbedPane jtMenu = new JTabbedPane();

        JPanel jpBase = new JPanel();
        jpBase.setLayout(new GridLayout(3,1));
        jpBase.add(new JPanel());

        // Base
        JPanel jpaux1 = new JPanel(new FlowLayout());
        jpaux1.add(new JLabel("Base imponible: "), BorderLayout.LINE_START);
        jtfBaseImponible = new JTextField();
        jtfBaseImponible.setPreferredSize(new Dimension(120, 28));
        jbBaseImponible = new JButton("Calcular factura");
        jbBaseImponible.setActionCommand("base");
        jpaux1.add(jtfBaseImponible);
        jpaux1.add(jbBaseImponible);

        jpBase.add(jpaux1);

        JPanel jpTotal = new JPanel();
        jpTotal.setLayout(new GridLayout(3,1));
        jpTotal.add(new JPanel());

        // Total Factura
        JPanel jpaux3 = new JPanel(new FlowLayout());
        jpaux3.add(new JLabel("Total factura"), BorderLayout.LINE_START);
        jtfTotalFactura = new JTextField();
        jtfTotalFactura.setPreferredSize(new Dimension(120, 28));
        jbTotalFactura = new JButton("Calcular con total factura");
        jbTotalFactura.setActionCommand("total");
        jpaux3.add(jtfTotalFactura);
        jpaux3.add(jbTotalFactura);

        jpTotal.add(jpaux3);

        jtMenu.add("Base imponible", jpBase);
        jtMenu.add("Total Factura", jpTotal);

        getContentPane().add(jtMenu, BorderLayout.CENTER);

        // Donem una mida a la finestra
        setSize(400, 250);
        setResizable(false);

        // Li posem un títol
        setTitle("Factura Calculator");

        // Indiquem que s'aturi el programa si fab clic a la "X" de la finestra
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        // Indiquem que la finestra se situï al centre de la pantalla
        setLocationRelativeTo(null);
    }

    public String getBase() {
        return jtfBaseImponible.getText();
    }

    public String getTotalFactura() {
        return jtfTotalFactura.getText();
    }


    public void showDetailsFactura(double base, double iva, double irpf, double total){
        JOptionPane.showMessageDialog(this,
                "BASE: " + base + "\nIVA: " + iva + "\nIRPF: " + irpf + "\nTOTAL: " + total);
    }


    public void assignButtonController(ButtonController b) {
        jbBaseImponible.addActionListener(b);
        jbTotalFactura.addActionListener(b);
    }
}
